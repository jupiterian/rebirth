// Fill out your copyright notice in the Description page of Project Settings.

#include "Rebirth.h"
#include "GlobalFunctionLibrary.h"

UObject* UGlobalFunctionLibrary::LoadObjectFromAssetPath(TSubclassOf<UObject> ObjectClass, FName Path, bool& IsValid)
{
	IsValid = false;

	if (Path == NAME_None) return NULL;
	UObject* LoadedObj = StaticLoadObject(ObjectClass, NULL, *Path.ToString());
	IsValid = LoadedObj != nullptr;
	return LoadedObj;
}

TArray<FString> UGlobalFunctionLibrary::ShuffleTextArray(FString Str, bool Print)
{
	TArray<FString> Parsed;
	Str.ParseIntoArrayLines(Parsed, true);

	for (int32 i = Parsed.Num() - 1; i > 0; i--) {
		int32 j = FMath::FloorToInt(FMath::SRand() * (i + 1)) % Parsed.Num();
		
		FString temp = Parsed[i];
		Parsed[i] = Parsed[j];
		Parsed[j] = temp;
	}

	if (Print) {
		for (int32 i = 0; i < Parsed.Num(); i++) {
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 500.0f, FColor::White, Parsed[i]);
		}
	}

	return Parsed;
}

FName UGlobalFunctionLibrary::GetBPClassName(TSubclassOf<AActor> Class)
{
	FString Name = Class->GetName();
	Name.RemoveFromEnd("_C");
	return FName(*Name);
}