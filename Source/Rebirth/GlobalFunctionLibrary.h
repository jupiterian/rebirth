// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "GlobalFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class REBIRTH_API UGlobalFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	

	UFUNCTION(BlueprintCallable, Category = "GlobalBPLibrary")
	static UObject* LoadObjectFromAssetPath(TSubclassOf<UObject> ObjectClass, FName Path, bool& IsValid);

	UFUNCTION(BlueprintCallable, Category = "GlobalBPLibrary")
	static TArray<FString> ShuffleTextArray(FString Str, bool Print);
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GlobalBPLibrary")
	static FName GetBPClassName(TSubclassOf<AActor> Class);
};
